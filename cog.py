from discord.ext import commands
from math import ceil
from random import choice
import discord
import datetime
from models import Quote
import typing
from utils import string_to_date
import io
from json import dumps

class Quotes(commands.Cog):


    @commands.Cog.listener()
    async def on_guild_leave(self, guild):
        for quote in Quote.objects(guild_id = guild.id):
            quote.delete()




    @commands.command(aliases = ["aqm"])
    async def addquotemanual(self, ctx, author : typing.Union[discord.Member, str], date : typing.Optional[string_to_date] = datetime.datetime.today().date(), *, text):
        """Add a manual quote."""

        text = text.replace("’","'")

        if isinstance(author, discord.Member):
            quote = Quote(text = text, author_name = author.display_name, author_id = author.id, date = date, guild_id = ctx.guild.id)

        else:
            quote = Quote(text = text, author_name = author, date = date, guild_id = ctx.guild.id)



        message = await ctx.send(embed=quote.embed)

        emoji = await ctx.wait_for_emoji(message=message, emojis=["✅","❎"], members=[ctx.author], add_reactions = True)

        if emoji == "✅":
            quote.save()
            await ctx.send("OK, added")
        else:
            quote.delete()
            await ctx.send("OK, cancelled") 




    @commands.command()
    async def export(self, ctx):
        quotes = []

        for quote in Quote.objects(guild_id = ctx.guild.id):
            quotes.append(quote.to_json())

        f = io.BytesIO()
        json.dump(quotes, f)
        print(f)


    @commands.command(aliases = ["aq"])
    @commands.guild_only()
    async def addquote(self, ctx, channel : typing.Optional[discord.TextChannel] = None, *, query):
        """Adds a quote to this servers' list of quotes."""

        if query.startswith(ctx.prefix):
            return await ctx.send("Quote can't be a command.")

        if channel is None:
            channel = ctx.channel

        perms = channel.permissions_for(ctx.guild.me)
        if not perms.read_messages and not perms.read_message_history:
            return await ctx.send("I cannot read this channel. Need the permissions: **Read Messages** and **Read Message History**")

        found = False
        async for message in channel.history(limit=500):
            if query.lower().strip() in message.content.lower() and not message.author.bot and not message.content.startswith(ctx.prefix):
                found = True
                break

        if not found: 
            return await ctx.send("This quote was not found.")

        quote = Quote(
            text        = message.clean_content,
            author_name = message.author.display_name,
            author_id   = message.author.id,
            date        = string_to_date(str(message.created_at).split()[0]),
            guild_id    = ctx.guild.id)

        quote.save()

        await ctx.send(embed=quote.embed)



    @commands.command(aliases = ["q"])
    async def quote(self, ctx, member: typing.Optional[discord.Member] = None):
        """Shows a (random) quote from this server."""

        if member is None:
            quote = choice(Quote.objects(guild_id = ctx.guild.id))
        else:
            quote = choice(Quote.objects(guild_id = ctx.guild.id, author_id = member.id))

        if quote is None:
            await ctx.send("No quotes were found.")
        else:
            await ctx.send(embed=quote.embed)


    @commands.command(aliases = ["pq"])
    async def purgequotes(self, ctx, author : discord.Member = None):
        """Purges some quotes."""


        if author is not None:
            if ctx.author.guild_permissions.administrator:
                quotes = list(Quote.object(guild_id = ctx.guild, author_id=author.id))
            else:
                return await ctx.send("You need the administrator permission to remove others quotes")

        else:
            if ctx.author.guild_permissions.administrator:
                quotes = list(Quote.object(guild_id = ctx.guild))
            else:
                quotes = list(Quote.object(guild_id = ctx.guild, author_id = ctx.author.id ))


        count = 0
        embeds = []

        for quote in quotes:

            if count % 10 == 0:
                embeds.append(discord.Embed())
                embeds[-1].set_footer(text=f"page: {len(embeds)}/{ceil(len(quotes) / 10)}")

            embed = embeds[-1]

            embed.add_field(name=f"{(count % 10)+1}: {quote.text} ", value = str(quote.author), inline = False)

            count += 1



        msg = await ctx.send(embed=embeds[-1])

        current_page = len(embeds)

        emojis = ("⬅", "1⃣","2⃣", "3⃣", "4⃣", "5⃣", "6⃣", "7⃣", "8⃣", "9⃣", "🔟", "➡", "✅")

        def check(r,a):
            return emojis.index(str(r.emoji)) in range(1, len(embeds[current_page-1].fields)+1 ) or str(r.emoji) in ("⬅","➡","✅" )

        page_emojis = {"➡": 1, "⬅" : -1}

        quotes_to_delete = []


        emoji = await ctx.wait_for_emoji(message=msg, emojis = emojis, members = (ctx.author,), check = check, remove_after = True, add_reactions = True)

        while emoji not in ("✅", None):

            emoji = await ctx.wait_for_emoji(message=msg, emojis = emojis, members = (ctx.author,), check = check, remove_after = True)

            if emoji in page_emojis:
                current_page += page_emojis[emoji]

                if current_page > len(embeds):
                    current_page = 1
                elif current_page < 1:
                    current_page = len(embeds)

                await msg.edit(embed=embeds[current_page-1])


            elif emoji not in ("✅", None):
                num   = emojis.index(emoji)-1
                quote = quotes[int(f"{current_page-1}{num}")]
                embed = embeds[current_page-1]
                field = embed.fields[num]

                if field.name.startswith("~~"):
                    embed.set_field_at(num, name=field.name[2:-2], value=field.value[2:-2], inline=False)
                    quotes_to_delete.remove(quote)
                else:
                    embed.set_field_at(num, name=f"~~{field.name}~~", value=f"~~{field.value}~~", inline=False)
                    quotes_to_delete.append(quote)
                await msg.edit(embed=embed)

        else:
            if len(quotes_to_delete) == 0:
                return await msg.edit(content="Okay. Nothing deleted.", embed=None)

            for quote in quotes_to_delete:
                quote.delete()

            await msg.edit(content="Okay. deleted these quotes.", embed=None)

            try:
                await msg.clear_reactions()
            except: pass


def setup(bot):
    bot.add_cog(Quotes(bot))
