#!/usr/bin/env python3.6
import asyncio

import discord

from utils import wait_for_emoji, wait_for_message

from bot import QuoteBot
bot = QuoteBot()

BOT_ADMINS = []
BOT_NAME = "Quote"

DISCORD_TOKEN = ""

@bot.event
async def on_ready():
    print("Ready.")

    app = await bot.application_info()
    if app.owner.id not in BOT_ADMINS:
        BOT_ADMINS.append(app.owner.id)



@bot.before_invoke
async def before_any_command(ctx):

    if ctx.guild == None:
        raise Exception("PM!")

    ctx.wait_for_emoji   = wait_for_emoji
    ctx.wait_for_message = wait_for_message



bot.run(DISCORD_TOKEN)
