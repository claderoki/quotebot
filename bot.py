from discord.ext import commands

class QuoteBot(commands.Bot):
    instance = None

    def __init__(self):
        super().__init__(command_prefix = "!")
        self.load_extension("cog")
        self.remove_command("help")

        self.__class__.instance = self
