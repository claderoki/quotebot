import discord

from random import choice

from mongoengine import connect, Document, StringField, IntField, DateField
from bot import QuoteBot


connect('quotebot')

class Quote(Document):
    text        = StringField (required = True )
    author_name = StringField (required = True )
    author_id   = IntField    (required = False)
    date        = DateField   (required = True )
    guild_id    = IntField    (required = True )

    @property
    def guild(self):
        return QuoteBot.instance.get_guild(self.guild_id)

    @property
    def author(self):
        if self.guild is not None:
            return self.guild.get_member(self.author_id)

    @property
    def embed(self) -> discord.Embed:

        embed = discord.Embed(color=0x8ec07c if self.author is None else self.author.color, title=self.text)

        if self.author is not None:
            embed.set_author(name=self.author.display_name, icon_url=self.author.avatar_url_as(format="jpeg", size=32))
        else:
            embed.set_author(name=self.author_name)

        embed.set_footer(text=self.date.isoformat())

        return embed
