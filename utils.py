import datetime


async def wait_for_emoji(message,
    timeout       = 360,
    emojis        = (),
    check         = lambda r,a : True,
    author        = None,
    members       = (),
    remove_after  = True,
    add_reactions = False,
    clear_after   = False):
    
    if author is not None and author not in members:
        members = list(members)
        members.append(author)


    if add_reactions:
        for emoji in emojis:
            await message.add_reaction(emoji)

    try:
        r, a = await CLIENT.wait_for('reaction_add', timeout=timeout,
            check=lambda r, a: check(r,a)
            and (a.id in [x.id for x in members] if len(members) > 0 else True)
            and r.message.id == message.id
            and (str(r.emoji) in emojis if len(emojis) > 0 else True)
            and not a.bot )
        emoji = str(r.emoji)

    except asyncio.TimeoutError:
        emoji = None

    if clear_after:
        try:
            await message.clear_reactions()
        except: pass

    elif remove_after:
        try:
            await message.remove_reaction(r, a)
        except: pass

    return emoji


async def wait_for_message(channel, timeout=360, check = lambda m: True, author = None, members = () ):
    
    if author is not None and author not in members:
        members = list(members)
        members.append(author)

    try:
        message = await CLIENT.wait_for('message',timeout=timeout,
        check=lambda m: check(m)
        and (m.author.id in [x.id for x in members] if len(members) > 0 else True)
        and channel.id == m.channel.id)
    except asyncio.TimeoutError:
        return None
    else:
        return message





def string_to_date(text) -> datetime.date:
    if text.count("-") == 2:
        dates = [int(x) for x in text.split("-")]
    elif text.count("/") == 2:
        dates = [int(x) for x in text.split("/")]
    elif text == "today":
        return datetime.now().date().strftime("%Y-%m-%d")
    else:
        raise Exception("Not correct.")

    if dates[1] > 12:
        raise Exception("Incorrect format.")

    if len(str(dates[0])) == 4:
        year,month,day = dates
    else:
        day,month,year = dates

    return datetime.date(year, month, day)
